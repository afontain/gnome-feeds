<nav>

- [<span>![](./logo-symbolic.svg) **Feeds**</span>](#)
- [Features](#features)
- [Install](#install)
- [Hack](#hack)
- [Support](#support)

</nav>

<!--@MARGIN@-->

![Logo](logo.svg)

# Feeds

## News reader for GNOME

![Main Window](screenshots/mainwindow.png)

---

## Features

### Simple User Experience

Add your favorite feeds, and start reading the latest news.

![Add](screenshots/add_feed.png)

### Read, unread

Read articles are grayed out. Mark them as read or unread with a simple right click or longpress.

![Read articles](screenshots/unread.png)

### Read what matters

Filter the articles by feed and only read what you want to read.

![Filter](screenshots/filter.png)

### On the go

Save any article you want and read it offline anytime and anywhere.

![Saved articles](screenshots/saved_articles.png)

### Easy on the eyes

Cut the clutter of modern websites with a large range of reading options!

Use reader mode to get to the juicy bits of an article.

![Reader](screenshots/reader.png)

Use the RSS content to glance through the essentials.

![RSS Content](screenshots/rss_content.png)

Load a full web view (with JavaScript disabled by default) to get the full experience.

![Web View](screenshots/webview.png)

### Large feed collection? Import it

Feeds supports importing and exporting your feed collection from and to OPML so that you can get set up in seconds.

![Import and Export](screenshots/import_export.png)

### Have it your way

Customize your experience with a large range of preferences.

![Preferences](screenshots/preferences.png)

### Responsive

Feeds is built with Purism's libhandy library to offer a responsive user interface. Got a new shiny GNU+Linux phone? Here's a feed reader for you!

<video autoplay mute controls loop>
    <source src="screenshots/responsive_demo.mp4" type="video/mp4" />
</video>

---

## Install

### Flatpak (recommended)

[Install **Flatpak** by following the quick setup guide](https://flatpak.org/setup/). Does it work on your distribution? Most likely.

Then, click the button below to install Feeds:

<a href="https://flathub.org/apps/details/org.gabmus.gnome-feeds"><img src="https://raw.githubusercontent.com/flatpak-design-team/flathub-mockups/master/assets/download-button/download.svg?sanitize=true" height="100" alt="Get it on Flathub" /></a>

### Fedora

Feeds is available on Fedora's official repos. Just run `sudo dnf install gnome-feeds` in your terminal to install it. Alternatively, use [Flatpak](#flatpak-recommended) to get the latest and greatest version of Feeds.

### AUR

If you're using Arch Linux or an Arch based system, you can install the [`gnome-feeds-git`](https://aur.archlinux.org/packages/gnome-feeds-git/) package from the AUR.

---

## Hack

![Code](icons/code.svg)

Feeds is written using Python 3 and GTK+ 3. It's free software, released under the GPL3 license. Feel free to browse the source code on [the GitLab repository](https://gitlab.com/gabmus/gnome-feeds), fork it, make changes or open issues!

---

## Support

![Bug](icons/bug.svg)

Have you found a bug? Do you want a new feature? Whatever the case, opening an issue is never a bad idea. You can do that on [the issue page of Feeds' GitLab repository](https://gitlab.com/gabmus/gnome-feeds/issues)
